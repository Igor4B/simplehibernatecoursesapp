package com.jpamanager;

import java.util.List;

public interface GenericReadOnlyDAO<T> {
	public T find(Integer id);
	public List<T> findAll();
}

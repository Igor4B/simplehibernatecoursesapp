package com.jpamanager;

public interface GenericDAO<T> extends GenericReadOnlyDAO<T> {
	public void save(T t);
	public void remove(T t);
}

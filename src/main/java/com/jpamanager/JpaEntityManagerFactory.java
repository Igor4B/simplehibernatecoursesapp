package com.jpamanager;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JpaEntityManagerFactory {

	private static final JpaEntityManagerFactory instance = new JpaEntityManagerFactory();
	private final EntityManagerFactory entityManagerFactory;

	private JpaEntityManagerFactory() {
		entityManagerFactory = Persistence.createEntityManagerFactory("persistance");
	}

	public static EntityManager getEntityManager() {
		return getInstance().entityManagerFactory.createEntityManager();
	}

	private static JpaEntityManagerFactory getInstance() {
		return instance;
	}
}

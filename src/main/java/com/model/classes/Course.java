package com.model.classes;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;




@Entity
@Table(name="course")
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "name", nullable = false, length = 100)
	private String name;
	
	@Column(name = "begins", nullable = false, length = 100)
	private String begins;
	
	@Column(name = "ends", nullable = false, length = 100)
	private String ends;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="courses_students",
	joinColumns=@JoinColumn(name="course_id"),
	inverseJoinColumns = @JoinColumn(name = "teacher_id"))
	private List<Teacher> teacher;
	
	@ManyToMany (cascade = CascadeType.ALL)
	@JoinTable(name = "courses_students",
    joinColumns = @JoinColumn(name = "course_id"),
    inverseJoinColumns = @JoinColumn(name = "student_id"))
	private List<Student> student;

	public Course() {
		super();
	}

	public Course(String name, String begins, String ends) {
		super();
		this.name = name;
		this.begins = begins;
		this.ends = ends;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBegins() {
		return begins;
	}

	public void setBegins(String begins) {
		this.begins = begins;
	}

	public String getEnds() {
		return ends;
	}

	public void setEnds(String ends) {
		this.ends = ends;
	}

	public List<Teacher> getTeacher() {
		return teacher;
	}

	public void setTeacher(List<Teacher> teacher) {
		this.teacher = teacher;
	}

	public List<Student> getStudent() {
		return student;
	}

	public void setStudent(List<Student> student) {
		this.student = student;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Course other = (Course) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Course [id=" + id + ", name=" + name + ", begins=" + begins + ", ends=" + ends + ", teacher=" + teacher
				+ ", student=" + student + "]";
	}
	
	
	
	
}

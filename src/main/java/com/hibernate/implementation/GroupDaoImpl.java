package com.hibernate.implementation;

import java.util.List;

import javax.persistence.EntityManager;

import com.jpamanager.GenericDAO;
import com.jpamanager.JpaEntityManagerFactory;
import com.model.classes.Group;


public class GroupDaoImpl implements GenericDAO<Group> {

	@Override
	public Group find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		Group result = em.find(Group.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<Group> findAll() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Group> result = em.createQuery("from Group", Group.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(Group t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
		
	}

	@Override
	public void remove(Group t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
		
	}

}

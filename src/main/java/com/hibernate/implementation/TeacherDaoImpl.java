package com.hibernate.implementation;

import java.util.List;

import javax.persistence.EntityManager;

import com.jpamanager.GenericDAO;
import com.jpamanager.JpaEntityManagerFactory;
import com.model.classes.Teacher;

public class TeacherDaoImpl implements GenericDAO<Teacher> {

	@Override
	public Teacher find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		Teacher result = em.find(Teacher.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<Teacher> findAll() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Teacher> result = em.createQuery("from Teacher", Teacher.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(Teacher t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
	}

	@Override
	public void remove(Teacher t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();

	}

}

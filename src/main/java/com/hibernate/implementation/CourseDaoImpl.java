package com.hibernate.implementation;

import java.util.List;

import javax.persistence.EntityManager;

import com.jpamanager.GenericDAO;
import com.jpamanager.JpaEntityManagerFactory;
import com.model.classes.Course;

public class CourseDaoImpl implements GenericDAO<Course> {

	@Override
	public Course find(Integer id) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		Course result = em.find(Course.class, id);
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public List<Course> findAll() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Course> result = em.createQuery("from Course", Course.class).getResultList();
		em.getTransaction();
		em.close();
		return result;
	}

	@Override
	public void save(Course t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();

	}

	@Override
	public void remove(Course t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
	}

}

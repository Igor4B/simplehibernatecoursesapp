package com.hibernate.implementation;

import java.util.List;

import javax.persistence.EntityManager;

import com.jpamanager.GenericDAO;
import com.jpamanager.JpaEntityManagerFactory;
import com.model.classes.Student;


public class StudentDaoImpl implements GenericDAO<Student>{

	@Override
	public Student find(Integer id) {
		
		return null;
	}

	@Override
	public List<Student> findAll() {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		List<Student> result = em.createQuery("from Student", Student.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return result;
	}

	@Override
	public void save(Student t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.persist(t);
		em.getTransaction().commit();
		em.close();
		
	}

	@Override
	public void remove(Student t) {
		EntityManager em = JpaEntityManagerFactory.getEntityManager();
		em.getTransaction().begin();
		em.remove(t);
		em.getTransaction().commit();
		em.close();
		
	}

}

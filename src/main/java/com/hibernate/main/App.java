package com.hibernate.main;

import java.util.ArrayList;
import java.util.List;

import com.hibernate.implementation.CourseDaoImpl;
import com.hibernate.implementation.GroupDaoImpl;
import com.jpamanager.GenericDAO;
import com.model.classes.Course;
import com.model.classes.Group;
import com.model.classes.Student;

public class App {

	public static void main(String[] args) {

		Course course = new Course("Java", "Tuesday 27-01-2018", "Friday 27-04-2018");
		GenericDAO<Course> coursedao = new CourseDaoImpl();
		coursedao.save(course);

		Group group = new Group("First group...");
		GenericDAO<Group> bookDao = new GroupDaoImpl();
		bookDao.save(group);

		Student student1 = new Student("Ilce", "Perov");
		Student student2 = new Student("Igor", "Gjorev");

		List<Student> students = new ArrayList<>();
		students.add(student1);
		students.add(student2);

	}

}
